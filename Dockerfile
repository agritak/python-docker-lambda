FROM public.ecr.aws/lambda/python:3.12

WORKDIR ${LAMBDA_TASK_ROOT}

COPY requirements.txt .

# Install the specified packages
RUN pip install -r requirements.txt

# Copy function code
COPY lambda_function.py .

# Set the CMD to lambda handler
CMD [ "lambda_function.handler" ]